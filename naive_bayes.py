import numpy as np
import math
from scipy.stats import norm
import numpy_indexed as npi
from collections import defaultdict, OrderedDict
from sklearn.base import BaseEstimator


class NaiveBayesNominal:
    def __init__(self):
        self.classes_ = None
        self.model = dict()
        self.y_prior = []

    def fit(self, X, y):
        y_type, y_value = np.unique(y, return_counts=True)
        self.y_prior = dict(zip(y_type, [count / len(y) for count in y_value]))

        data_dict = dict()
        for row_index, row in enumerate(X):
            for attr_index, attr_value in enumerate(row):
                key = (attr_index, attr_value, y[row_index])
                if key not in data_dict.keys():
                    data_dict[key] = 1
                else:
                    data_dict[key] += 1

        for key, value in data_dict.items():
            for type in y_type:
                if key[2] == type:
                    data_dict[key] /= y_value[np.where(y_type==type)]

        self.model = data_dict

    def get_attr_prior_probabilities(self, given_class):
        attr_priors = dict()
        attr_priors_mean = dict()

        for key, value in self.model.items():
            if key[2] == given_class:
                if key[0] not in attr_priors.keys():
                    attr_priors[key[0]] = []
                    attr_priors[key[0]].append(value)
                else:
                    attr_priors[key[0]].append(value)

        for key, value in attr_priors.items():
            attr_priors_mean[key] = np.mean(attr_priors[key])

        correct_list_sorted = OrderedDict(sorted(attr_priors_mean.items(), key=lambda t: -t[1]))
        n_best_correct_list_sorted = {k: correct_list_sorted[k] for k in list(correct_list_sorted)[:3]}
        return n_best_correct_list_sorted

