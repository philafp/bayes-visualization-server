import json
from collections import Counter
from collections import OrderedDict


import pandas as pd
from flask import Flask, request
from flask_cors import CORS
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from pandas_ml import ConfusionMatrix
import numpy as np


from naive_bayes import NaiveBayesNominal

app = Flask(__name__)
CORS(app)


@app.route('/chart1', methods=['POST'])
def train_by_Bayes():
    file = request.files['csvFile']
    global dataset
    dataset = pd.read_csv(file)

    X = dataset.iloc[:, 1:].values
    y = dataset.iloc[:, 0].values

    global y_test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    classifier = GaussianNB()
    classifier.fit(X_train, y_train)

    global y_pred
    y_pred = classifier.predict(X_test)

    count_dict = Counter(y_test)
    target_names = list(count_dict.keys())
    report = classification_report(y_test, y_pred, target_names=target_names)

    lines = report.split('\n')
    class_precision = dict()

    for line in lines[2:-5]:
        row_data = line.split(' ')
        row_data = list(filter(None, row_data))
        class_precision[row_data[0]] = float(row_data[2]) * 100

    list_of_jsons = []

    for key, value in class_precision.items():
        correct = round(count_dict[key] * value / 100)
        incorrect = count_dict[key] - correct
        data = {'label': key, 'value': count_dict[key], 'percent': round(count_dict[key] / len(y_test) * 100, 1),
                'inner': [{'label': 'correct', 'value': correct, 'percent': round(correct / count_dict[key] * 100, 1)},
                          {'label': 'incorrect', 'value': incorrect, 'percent': round(incorrect / count_dict[key] * 100, 1)}]}
        list_of_jsons.append(data)

    return json.dumps(list_of_jsons)


@app.route('/legend', methods=['POST'])
def legend():
    file = request.files['csvFile']
    dataset = pd.read_csv(file)

    X = dataset.iloc[:, 1:].values
    y = dataset.iloc[:, 0].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    classifier = GaussianNB()
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    score = accuracy_score(y_test, y_pred)
    data = {'train_set': len(X_train), 'test_set': len(X_test), 'set_name': file.filename, 'score': score}
    return json.dumps(data)


@app.route('/confusionMatrix&givenClass=<given_class>', methods=['POST'])
def train_by_Bayes_for_confusion_matrix(given_class):
    cnf_matrix = ConfusionMatrix(y_test, y_pred)
    cn_matrix = confusion_matrix(y_test, y_pred)
    # cn_matrix[:, 0]
    list = cnf_matrix.classes.values
    index = np.where(list == given_class)[0][0]
    column = [row[index] for row in cn_matrix]
    row = cn_matrix[index, :]
    matrix = np.full((len(list), len(list)), -1)
    matrix[:, index] = column
    matrix[index, :] = row
    data = {'data': matrix.tolist(), 'labels': list.tolist()}
    return json.dumps(data)


@app.route('/chart2&givenClass=<given_class>', methods=['POST'])
def train_by_Bayes_for_2_chart(given_class):
    X = dataset.iloc[:, 1:].values
    y = dataset.iloc[:, 0].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    classifier = NaiveBayesNominal()
    classifier.fit(X_train, y_train)
    best_attr_with_values = classifier.get_attr_prior_probabilities(given_class)
    list_of_attr_to_json = []
    for key, value in best_attr_with_values.items():
        data = {'attr_name': key, 'attr_value': value*100}
        list_of_attr_to_json.append(data)

    return json.dumps(list_of_attr_to_json)



if __name__ == '__main__':
    app.run(host='0.0.0.0')